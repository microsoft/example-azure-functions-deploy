# Bitbucket Pipelines Pipe Sample: Azure Functions Deploy (Node.js)

This sample uses the Bitbucket Azure Functions Deploy pipeline to deploy the [Echo Function](https://github.com/hannesne/EchoFunction/tree/master/JavaScript/V2) to [Azure Functions](https://docs.microsoft.com/en-us/azure/azure-functions/functions-overview). The application has been built using Node.

## Prerequisites

You will need to configure required Azure resources before running the pipe. The easiest way to do it is by using the Azure cli. You can either [install the Azure cli](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest) on your local machine, or you can use the [Azure Cloud Shell](https://docs.microsoft.com/en-us/azure/cloud-shell/overview) provided by the Azure Portal in a browser.

### Service principal

You will need a service principal with sufficient access to create an Azure Functions app, or update an existing Functions app. To create a service principal using the Azure CLI, execute the following command in a bash shell:

```sh
az ad sp create-for-rbac --name MyServicePrincipal
```

Refer to the following documentation for more detail:

* [Create an Azure service principal with Azure CLI](https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli)

### Azure Functions app

Using the service principal credentials obtained in the previous step, you can use the following commands to create an Azure Functions instance in a bash shell:

```bash
az login --service-principal --username ${AZURE_APP_ID}  --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}

az group create --name ${AZURE_RESOURCE_GROUP} --location australiaeast

az storage account create --name ${FUNCTION_APP_STORAGE_NAME} --location australiaeast --resource-group ${AZURE_RESOURCE_GROUP} --sku Standard_LRS

az functionapp create --name ${FUNCTION_APP_NAME} --resource-group ${AZURE_RESOURCE_GROUP} --storage-account ${FUNCTION_APP_STORAGE_NAME}
```

Refer to the following documentation for more detail:

* [Create your first function from the command line](https://docs.microsoft.com/en-us/azure/azure-functions/functions-create-first-azure-function-azure-cli)

## Support

This sample is provided "as is" and is not supported. Likewise, no commitments are made as to its longevity or maintenance. To discuss this sample with other users, please visit the Azure DevOps Services section of the Microsoft Developer Community: https://developercommunity.visualstudio.com/spaces/21/index.html.
